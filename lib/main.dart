import 'package:codexlibertas/modules/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme(
          brightness: Brightness.light,
          primary: Colors.teal,
          onPrimary: Colors.teal.shade400,
          secondary: Colors.blueGrey,
          onSecondary: Colors.blueGrey.shade400,
          error: Colors.red,
          onError: Colors.red.shade400,
          background: Colors.white24,
          onBackground: Colors.white54,
          surface: Colors.lightBlue,
          onSurface: Colors.lightBlue.shade400,
        ),
      ),
      home: const AnimSplash(),
    );
  }
}
