import 'package:codexlibertas/modules/dashboard/explorescreen.dart';
import 'package:codexlibertas/modules/dashboard/homescreen.dart';
import 'package:codexlibertas/modules/dashboard/profilescreen.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DashboardView extends StatefulWidget {
  const DashboardView({Key? key}) : super(key: key);

  @override
  State<DashboardView> createState() => _DashboardViewState();
}

class _DashboardViewState extends State<DashboardView> {
  int currentView = 0;

  final List<Widget> avViews = [
    const HomeView(),
    const ExploreView(),
    const ProfileView(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: currentView,
        children: avViews,
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.blueGrey,
        selectedItemColor: Colors.black,
        unselectedItemColor: Colors.white,
        iconSize: 25,
        currentIndex: currentView,
        // showSelectedLabels: false,
        showUnselectedLabels: false,
        onTap: (index) => setState(() => currentView = index),
        items: const [
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.house),
            label: 'home',
          ),
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.compass),
            label: 'explore',
          ),
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.personBooth),
            label: 'profile',
          ),
        ],
      ),
    );
  }
}
