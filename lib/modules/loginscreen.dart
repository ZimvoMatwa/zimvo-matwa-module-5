import 'package:codexlibertas/modules/dashboard/dashboard.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class LoginScreenView extends StatelessWidget {
  const LoginScreenView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login'),
        centerTitle: true,
      ),
      body: Container(
        padding: const EdgeInsets.all(30),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'Sign in to Twitter',
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 28,
                ),
              ),
              const SizedBox(height: 30),
              socialButton(context,
                  icon: FontAwesomeIcons.google, socialName: 'Google'),
              socialButton(context,
                  icon: FontAwesomeIcons.apple, socialName: 'Apple'),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    height: 3,
                    color: Colors.grey,
                    width: MediaQuery.of(context).size.width * .4,
                  ),
                  const Text('or'),
                  Container(
                    height: 3,
                    color: Colors.grey,
                    width: MediaQuery.of(context).size.width * .4,
                  ),
                ],
              ),
              const SizedBox(height: 20),
              const TextField(
                decoration: InputDecoration(
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                  hintText: 'Phone, email or username',
                  border: OutlineInputBorder(borderSide: BorderSide(width: 1)),
                ),
              ),
              const SizedBox(height: 20),
              socialButton(
                context,
                socialName: 'Next',
                color: Colors.black,
                textColor: Colors.white,
                pageRoute: true,
              ),
              socialButton(context, socialName: 'Forgot Password'),
              const SizedBox(height: 50),
              RichText(
                text: const TextSpan(
                  text: "Don't have an account? ",
                  style: TextStyle(color: Colors.black),
                  children: [
                    TextSpan(
                      text: 'Sign up',
                      style: TextStyle(color: Colors.lightBlue),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

Widget socialButton(BuildContext context,
    {IconData? icon,
    required String socialName,
    Color? color,
    Color? textColor,
    bool pageRoute = false}) {
  return InkWell(
    borderRadius: BorderRadius.circular(100),
    onTap: () {
      pageRoute
          ? Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const DashboardView()),
            )
          : null;
    },
    child: Container(
      margin: const EdgeInsets.only(bottom: 15),
      width: MediaQuery.of(context).size.width * 8,
      height: 40,
      decoration: BoxDecoration(
        color: color ?? Colors.transparent,
        border: Border.all(color: Colors.blueGrey, width: 2),
        borderRadius: BorderRadius.circular(100),
      ),
      child: icon != null
          ? Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(icon),
                const SizedBox(width: 8),
                Text('Sign in with $socialName'),
              ],
            )
          : Center(
              child: Text(
                socialName,
                style: TextStyle(color: textColor ?? Colors.black),
              ),
            ),
    ),
  );
}
