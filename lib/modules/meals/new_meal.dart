import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class NewMealView extends StatefulWidget {
  const NewMealView({Key? key}) : super(key: key);

  @override
  State<NewMealView> createState() => _NewMealViewState();
}

class _NewMealViewState extends State<NewMealView> {
  final controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextField(
          controller: controller,
        ),
        actions: [
          IconButton(
              onPressed: () async {
                final name = controller.text;

                createMeal(mealName: name);
              },
              icon: const Icon(Icons.add_a_photo))
        ],
      ),
    );
  }

  Future createMeal(
      {required String
          mealName /*, required String firstIngredient, required String secIngredient, required String timeOfDayType*/}) async {
    final docMeal =
        FirebaseFirestore.instance.collection('meals').doc(mealName);

    final json = {
      // 'ingredients': [firstIngredient, secIngredient],
      // 'timeofdaytype': timeOfDayType,
      'ingredients': ['bread', 'peanut butter and jelly'],
      'timeOfDayType': 'anytime'
    };

    /// create doc and write data to firebase
    await docMeal.set(json);
  }
}
