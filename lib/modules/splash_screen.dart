import 'dart:async';

import 'package:codexlibertas/modules/loginscreen.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class AnimSplash extends StatelessWidget {
  const AnimSplash({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Timer(
      const Duration(seconds: 3),
      () => Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (BuildContext context) => const LoginScreenView(),
        ),
      ),
    );
    return Scaffold(
      body: Center(
        child: Lottie.asset('assets/splash/splash.json', repeat: false),
      ),
    );
  }
}
